///////////////////BANNER ACTION//////////////////////////////////////////////
imgs = document.querySelectorAll(".banner .banner-item");
slideLength = (imgs.length) - 1;
globalCount = 0;


function initSlider() {

  for (i = 0; i < imgs.length; i++) {
    imgs.item(0).style.display = "inherit";
    if (i !== 0) {
      imgs.item(i).style.display = "none";
    }
  }


}

function slide() {
  globalCount === slideLength ? globalCount = 0 : globalCount++;
  if (globalCount === 0) {
    imgs.item(globalCount).style.display = "inherit";
    imgs.item(slideLength).style.display = "none";
  }
  else {
    imgs.item(globalCount).style.display = "inherit";
    imgs.item(globalCount - 1).style.display = "none";
  }
}

initSlider();
setInterval('slide()', 5000);

////////////////////////CLIENT SPEECH/////////////////////////////////////////
speech = document.querySelectorAll(".testimonial p");
speechLength = (speech.length) - 1;
initid = 0;

function testimonialInit() {

  for (i = 0; i < speech.length; i++) {
    speech.item(0).style.display = "inline";
    if (i !== 0) {
      speech.item(i).style.display = "none";
    }
  }

}

function testimonial(element) {

  clientSelected = element.id - 1;

  document.querySelectorAll(".testimonial p")
      .item(initid).style.display = 'none';
  document.querySelectorAll(".testimonial p")
      .item(clientSelected).style.display = 'inline';
  initid = clientSelected;

}

testimonialInit();