<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Webwise | <?php echo $content->data['header']['title'];?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="app/front/css/normalize.css">
    <link rel="stylesheet" href="app/front/css/main.css">
</head>
<body>
<!--******************************************************************************
****                                  CONTAINER                               ****
**********************************************************************************-->
<div class="container row">
<?php

 include $dir.'/app/views/Header.php';
 include $dir.'/app/views/MainContent.php';
 include $dir.'/app/views/ComplementaryContent.php';
 include $dir.'/app/views/Footer.php';

?>

</div>
<script src="app/front/js/main.js"></script>
</body>
</html>


    

