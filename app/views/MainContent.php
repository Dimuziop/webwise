<!--******************************************************************************
****                                  MAIN CONTENT                            ****
**********************************************************************************-->

<main role="main" class="row">

    <article class="col span_16 clr">

        <?php echo $content->data['banner'] ?>
        
    </article>

    <aside role="complementary" class="col span_8 clr ">

        <div class="row decorated">
            <h4><?php echo $content->data['aside']->getTitle(); ?></h4>
            <p class="main">
                <?php echo $content->data['aside']->getParagraph(); ?>
            </p>
            <p class="complement">
                <?php echo $content->data['aside']->getParagraph2(); ?>
                <a href="<?php echo $content->data['aside']->getLink(); ?>">more</a>
            </p>
        </div>
        <div class="row client clr">
            <h4>Clients we serve..</h4>
            <?php
            foreach ($content->data['testimonies'] as $testimony){
                echo '<img onclick="testimonial(this)" src="'.$testimony->getPic().'" alt="'.$testimony->getClient().'" id="'.$testimony->getId().'">';
            }
             ?>
        </div>

    </aside>

</main>
    
