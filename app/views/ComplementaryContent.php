<!--******************************************************************************
****                        COMPLEMENTARY CONTENT                             ****
**********************************************************************************-->
<div class="row gap"></div>
<div role="complementary" class="row">

    <aside role="complementary"
           class="col span_8 clr decorated no-bottom--border">
        <h4><?php echo $content->data['specialized']->getTitle() ?></h4>
        <p class="main">
            <?php echo $content->data['specialized']->getDescription() ?>
        </p>
        <ul>
            <?php
            foreach ($content->data['specialized']->getLinks() as $links) {
                echo '<li>' . $links . '</li>';
            }
            ?>
        </ul>

    </aside>
    <article class="col span_16 clr">

        <div class="row ">
            <div class="col span_16 clr middle">
                <div class="testimonial">
                    <?php
                    foreach ($content->data['testimonies'] as $testimony) {
                        echo '<p id="' . $testimony->getId() . '">' . $testimony->getTestimonial() . '</p>';
                    }
                    ?>
                </div>

            </div>
            <div class="col span_8 clr special">
                <h3> Put us to work for you. We like to Serve</h3>
                <p>Use our Talk to Us form to drop us a note</p>
            </div>
        </div>

    </article>

</div>
<hr/>
    
