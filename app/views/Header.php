        <!--******************************************************************************
        ****                                  HEADER                                  ****
        **********************************************************************************-->
        <header class="row">
            <nav class="row decorated">
                <input class="trigger" type="checkbox" id="mainNavButton">
                <label for="mainNavButton" onclick>webWise</label>
                <ul>
                    <?php
                    foreach ($content->data['menu'] as $menuItem) {
                        echo $menuItem;
                    }?>
    
                </ul>
    
            </nav>
            <div class="row title-zone">
    
                <div class="col span_16">
                    <img src="<?php echo $content->data['header']['pic'];?>" alt="">
                </div>
    
                <div class="col span_8">
                    <h4 class="lower"><?php echo $content->data['header']['title'];?></h4>
                </div>
    
            </div>
    
    
        </header>
    
    
