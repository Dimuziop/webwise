<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 12/8/2017
 * Time: 02:01
 */

namespace Webwise\Controllers;


use Webwise\Models\AsideItem;
use Webwise\Models\BannerItem;
use Webwise\Models\DummyData\DummyData;
use Webwise\Models\MenuItem;
use Webwise\Models\SpecializeItem;
use Webwise\Models\TestimonyItem;
use Webwise\Views\MainView;

class MainController extends Controller {
    
    public $view;
    public $data = [];
    
    public function __construct() {
        parent::__construct();
        $this->controllerLogic();
    }
    
    //It needs to be refactorized with some class or classes who manage dependencies symplifing the method, facade.
    public function controllerLogic(){
        //menu section get and define
        $menu_items = DummyData::getMenuItems();
        $menu = $this->doMenu($menu_items);
        $this->setData('menu',$menu);
        
        //main content get and define
        $title = DummyData::getTitleItem();
        $this->setData('header',$title);
        
        //banner get and define
        $bannerContent = DummyData::getBannerItem();
        $banner = $this->doBanner($bannerContent);
        $renderedBanner = $this->injector['helpers']::bannerMaker($banner);
        $this->setData('banner', $renderedBanner);
        
        //aside get and define
        $aside_items = DummyData::getAsideContentItem();
        $aside = new AsideItem($aside_items['title'], $aside_items['paragraph1'], $aside_items['paragraph2'], $aside_items['link']);
        $this->setData('aside', $aside);
        
        //Client speech get and define
        $testymonyTable = DummyData::getClientTestimonyTable();
        $this->setData('testimonies', $this->doTestimonies($testymonyTable));
        
        //Specielize get and define
        $specialize_items = DummyData::getSpecializeItem();
        $specialize = new SpecializeItem($specialize_items['title'], $specialize_items['description'], $specialize_items['links']);
        $this->setData('specialized', $specialize);
        
        //TODO: refactorize and add dinamics for the footer, getting data from menu faq section with random on load
        

        
    }
    
    public function setData(string $key, $data){
        $this->data[$key] = $data;
    }
    
    // Eq getAll() form any ORM, or fetchAll() from query
    public function doMenu(array $menu_items){
        $i=0;
        foreach ($menu_items as $menu_item=>$item){
            $item = new MenuItem($item['title'], $item['tinny_title'], $item['url']);
            $menu[$i]= $item;
            $i++;
        }
        return $menu;
    }
    
    public function doBanner(array $banner_items){
        $i=0;
        foreach ($banner_items as $banner_item=>$item){
            $item = new BannerItem($item['pic'], $item['alt'], $item['title'], $item['description'], $item['link']);
            $banner[$i]= $item;
            $i++;
        }
        return $banner;
    }
    
    public function doTestimonies(array $testimony_items){
        $i=0;
        foreach ($testimony_items as $testimony_item=>$item){
            $item = new TestimonyItem($item['id'], $item['client'], $item['pic'], $item['testimony']);
            $testimony[$i]= $item;
            $i++;
        }
        return $testimony;
    }
    
    public function render(){
        echo $this->view;
    }
    
}