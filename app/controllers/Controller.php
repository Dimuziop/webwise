<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 13/8/2017
 * Time: 12:26
 */

namespace Webwise\Controllers;


use Webwise\Helpers\Helpers;

abstract class Controller {
    
    protected $injector = [];
    
    public function __construct(){
        $this->injector['helpers']= new Helpers();
    }
    
}