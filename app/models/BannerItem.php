<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 13/8/2017
 * Time: 12:03
 */

namespace Webwise\Models;


class BannerItem implements ModelInterface {
    
    protected $picture;
    protected $alt_text;
    protected $title;
    protected $description;
    protected $link;
    
    public function __construct($pic,$alt_text, $title, $description, $link) {
        
        $this->setPicture($pic);
        $this->setTitle($title);
        $this->setLink($link);
        $this->setAltText($alt_text);
        $this->setDescription($description);
    }
    
    public function __toString() {
        return '
        <div class="banner-item view view-first clr">
                <img class="clr" src="'.$this->getPicture().'" alt="'.$this->getAltText().'">
                <div class="mask clr">
                    <h2>'.$this->getTitle().'</h2>
                    <p>'.$this->getDescription().'.</p>
                    <a href="'.$this->getLink().'" class="info">Read More</a>
                </div>
            </div>
            ';
    }
    
    /**
     * @param mixed $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }
    
    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * @param mixed $alt_text
     */
    public function setAltText($alt_text) {
        $this->alt_text = $alt_text;
    }
    
    /**
     * @return mixed
     */
    public function getAltText() {
        return $this->alt_text;
    }
    
    /**
     * @param mixed $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }
    
    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }
    
    /**
     * @param mixed $link
     */
    public function setLink($link) {
        $this->link = $link;
    }
    
    /**
     * @return mixed
     */
    public function getLink() {
        return $this->link;
    }
    
    /**
     * @param mixed $picture
     */
    public function setPicture($picture) {
        $this->picture = $picture;
    }
    
    /**
     * @return mixed
     */
    public function getPicture() {
        return $this->picture;
    }
    
}