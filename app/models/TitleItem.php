<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 12/8/2017
 * Time: 07:31
 */

namespace Webwise\Models;


class TitleItem implements ModelInterface {
    
    protected $title;
    protected $pic;
    
    public function __toString() {
        return (string) $this->title;
    }
    
    /**
     * @return string
     */
    public function getPic() {
        return $this->pic;
    }
    
    /**
     * @param string $pic
     */
    public function setPic($pic) {
        $this->pic = $pic;
    }
    
    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }
    
}