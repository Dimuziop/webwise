<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 14/8/2017
 * Time: 00:12
 */

namespace Webwise\Models;



class LinkItem implements ModelInterface {
    
    protected $description;
    protected $link;
    protected $id;
    
    public function __construct($description, $link, $id) {
        $this->setDescription($description);
        $this->setId($id);
        $this->setLink($link);
    }
    
    public function __toString() {
        return '<a href="'.$this->getLink().'">'.$this->getDescription().'</a>';
    }
    
    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }
    
    /**
     * @param mixed $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }
    
    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }
    
    /**
     * @param mixed $link
     */
    public function setLink($link) {
        $this->link = $link;
    }
    
    /**
     * @return mixed
     */
    public function getLink() {
        return $this->link;
    }
    
}