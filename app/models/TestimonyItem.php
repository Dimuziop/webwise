<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 13/8/2017
 * Time: 21:09
 */

namespace Webwise\Models;


class TestimonyItem implements ModelInterface {
    
    protected $id;
    protected $client;
    protected $pic;
    protected $testimonial;
    
    public function __construct($id, $client, $pic, $testimonial) {
        
        $this->setId($id);
        $this->setClient($client);
        $this->setPic($pic);
        $this->setTestimonial($testimonial);
        
    }
    
    public function __toString() {
        return (string) $this->client;
    }
    
    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }
    
    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * @param mixed $pic
     */
    public function setPic($pic) {
        $this->pic = $pic;
    }
    
    /**
     * @return mixed
     */
    public function getPic() {
        return $this->pic;
    }
    
    /**
     * @return mixed
     */
    public function getClient() {
        return $this->client;
    }
    
    /**
     * @param mixed $client
     */
    public function setClient($client) {
        $this->client = $client;
    }
    
    /**
     * @return mixed
     */
    public function getTestimonial() {
        return $this->testimonial;
    }
    
    /**
     * @param mixed $testimonial
     */
    public function setTestimonial($testimonial) {
        $this->testimonial = $testimonial;
    }
    
}