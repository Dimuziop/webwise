<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 13/8/2017
 * Time: 19:42
 */

namespace Webwise\Models;


interface ModelInterface {
  
    public function __toString();
}