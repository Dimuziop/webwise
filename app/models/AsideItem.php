<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 13/8/2017
 * Time: 19:47
 */

namespace Webwise\Models;



class AsideItem implements ModelInterface {
    
    protected $title;
    protected $paragraph;
    protected $paragraph2;
    protected $link;
    
    public function __construct($title, $paragraph, $paragraph2, $link) {
        
        $this->setTitle($title);
        $this->setParagraph($paragraph);
        $this->setParagraph2($paragraph2);
        $this->setLink($link);
        
    }
    
    public function __toString() {
        return (string) $this->title;
    }
    
    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * @param mixed $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }
    
    /**
     * @return mixed
     */
    public function getParagraph() {
        return $this->paragraph;
    }
    
    /**
     * @param mixed $paragraph
     */
    public function setParagraph($paragraph) {
        $this->paragraph = $paragraph;
    }
    
    /**
     * @return mixed
     */
    public function getParagraph2() {
        return $this->paragraph2;
    }
    
    /**
     * @param mixed $paragraph2
     */
    public function setParagraph2($paragraph2) {
        $this->paragraph2 = $paragraph2;
    }
    
    /**
     * @return mixed
     */
    public function getLink() {
        return $this->link;
    }
    
    /**
     * @param mixed $link
     */
    public function setLink($link) {
        $this->link = $link;
    }
}