<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 12/8/2017
 * Time: 03:24
 *
 * Dummy data object
 * @Return array Data kind of type, NOT OBJECT, simulate DB returns.
 *
 * If needs data changes, do this here, like a CRM.
 */

namespace Webwise\Models\DummyData;



use Webwise\Models\LinkItem;

class DummyData {
    
    private static $menuItemTable;
    private static $titleItemTable;
    private static $bannerTable;
    private static $asideContentTable;
    private static $clientTestimonyTable;
    private static $specializeItem;
    
    public static function setDataMenu(){
        self::$menuItemTable =
            array (
            array(
                'title'=>'Home',
                'tinny_title'=>'frontpage',
                'url'=>'#'
            ),
            array(
                'title'=>'About',
                'tinny_title'=>'who we are',
                'url'=>'#'
            ),
            array(
                'title'=>'Services',
                'tinny_title'=>'what we offer',
                'url'=>'#'
            ),
            array(
                'title'=>'Studies',
                'tinny_title'=>'what we \'ve done',
                'url'=>'#'
            ),
            array(
                'title'=>'Testimonials',
                'tinny_title'=>'kudos by our clients',
                'url'=>'#'
            ),
            array(
                'title'=>'FAQ',
                'tinny_title'=>'questions?',
                'url'=>'#'
            ),
            array(
                'title'=>'Say Hello',
                'tinny_title'=>'note of a RFQ',
                'url'=>'#'
            )
        );
        self::$titleItemTable = array(
            'title' => 'building great communities powered by drupal',
            'pic' => 'app/front/img/webwise.png'
        );
        self::$bannerTable = array(
            'item1' => array(
                'pic' => 'http://lorempixel.com/750/490/',
                'alt' => 'lorem pixel test img',
                'title' => 'Some text about the img',
                'description' => 'description about img1',
                'link' => '#'
            ),
            'item2' => array(
                'pic' => 'http://lorempixel.com/749/490/',
                'alt' => 'lorem pixel test img2',
                'title' => 'Some text about the img2',
                'description' => 'description about img2',
                'link' => '#'
            ),
            'item3' => array(
                'pic' => 'http://lorempixel.com/751/490/',
                'alt' => 'lorem pixel test img3',
                'title' => 'Some text about the img3',
                'description' => 'description about img3',
                'link' => '#'
            )
        );
        self::$asideContentTable = array(
              'title' => 'Introducing...',
              'paragraph1' => 'WebWise solutions is dedicated to one goal,
                                making your customers insanely happy with your
                                products. Everything we do is designed to attract,
                                compel, delight, tantalize, and educate peopleso
                                they can´t imagine the life without you.',
              'paragraph2' => 'How do we do it? We have spent the last twelve
                                years creating corporate web sites and web
                                communities, and have learned what works and
                                what doesn\'t work',
              'link' => '#'
          
        );
        
        self::$clientTestimonyTable = array(
            'item1' => array(
                'pic' => 'http://lorempixel.com/100/100/',
                'id' => '1',
                'client' => 'Client 1',
                'testimony' => 'Testimony speech 1'
            ),
            'item2' => array(
                'pic' => 'http://lorempixel.com/101/100/',
                'id' => '2',
                'client' => 'Client 2',
                'testimony' => 'Testimony speech 2'
            ),
            'item3' => array(
                'pic' => 'http://lorempixel.com/99/100/',
                'id' => '3',
                'client' => 'Client 3',
                'testimony' => 'Testimony speech 3'
            )
        );
        
        self::$specializeItem = array(
            'title' => 'We specialize in:',
            'description' => 'We build engaging communities powered by
                            the powerfull open-source software, Drupal.',
            'links' => array(
                     new LinkItem('Full Services Packages', '#', '1' ),
                     new LinkItem('Editorial and Community Management Only', '#', '2' ),
                     new LinkItem('Communities On-Demand', '#', '3' )
            )
        );
        
    }
    
    public static function getMenuItems(){
        self::setDataMenu();
        return self::$menuItemTable;
    }
    
    public static function getTitleItem(){
        return self::$titleItemTable;
    }
    
    public static function getBannerItem(){
        return self::$bannerTable;
    }
    
    /**
     * @return mixed
     */
    public static function getAsideContentItem() {
        return self::$asideContentTable;
    }
    
    /**
     * @return mixed
     */
    public static function getClientTestimonyTable() {
        return self::$clientTestimonyTable;
    }
    
    /**
     * @return mixed
     */
    public static function getSpecializeItem() {
        return self::$specializeItem;
    }
    
}

