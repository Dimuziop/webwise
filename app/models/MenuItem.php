<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 12/8/2017
 * Time: 02:50
 */

namespace Webwise\Models;


class MenuItem implements ModelInterface {
    
    private $title;
    private $href;
    private $tinnyTitle;
    
    public function __construct($title, $tinnyTitle, $href) {
        $this->setTitle($title);
        $this->setTinnyTitle($tinnyTitle);
        $this->setHref($href);
    }
    
    public function __toString() {
        return
            '<li><a href="'.$this->getHref().'">'
            .$this->getTitle().
            '</a><span>'.$this->getTinnyTitle().'</span></li>';
    }
    
    /**
     * @param string $title
     */
    public function setTitle(string $title) {
        $this->title = $title;
    }
    
    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * @param string $tinnyTitle
     */
    public function setTinnyTitle(string $tinnyTitle) {
        $this->tinnyTitle = $tinnyTitle;
    }
    
    /**
     * @return string
     */
    public function getTinnyTitle() {
        return $this->tinnyTitle;
    }
    
    /**
     * @param string $href
     */
    public function setHref(string $href) {
        $this->href = $href;
    }
    
    /**
     * @return string
     */
    public function getHref() {
        return $this->href;
    }
    
}