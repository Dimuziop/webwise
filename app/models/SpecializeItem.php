<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 13/8/2017
 * Time: 23:54
 */

namespace Webwise\Models;


class SpecializeItem implements ModelInterface {
    
    protected $title;
    protected $description;
    protected $links = [];
    
    public function __construct($title, $description, array $links) {
        
        $this->setTitle($title);
        $this->setDescription($description);
        $this->setLinks($links);
        
    }
    
    public function __toString() {
       return (string) $this->title;
    }
    
    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * @param mixed $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }
    
    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }
    
    /**
     * @param mixed $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }
    
    /**
     * @return array
     */
    public function getLinks(): array {
        return $this->links;
    }
    
    /**
     * @param array $links
     */
    public function setLinks(array $links) {
        $this->links = $links;
    }
    
}