<?php
/**
 * Created by PhpStorm.
 * User: Patricio
 * Date: 13/8/2017
 * Time: 12:22
 */

namespace Webwise\Helpers;


class Helpers {
    
    public static function bannerMaker ($banner){
    
        $line1 = '<div class="banner clr">';
        $lastLine = '</div>';
        $ban = '';
        
        foreach ($banner as $item_banner){
            $ban .= $item_banner;
        }
        return $line1.$ban.$lastLine;
    }
    
}