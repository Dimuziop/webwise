# Webwise

This is a test project this is a test project that implements php to process dynamically to process dynamically 
webwise front-end using MVC design pattern.

##What's inside

It´s a minified test example CRM

* Controller folder contains app MainController example

* Models folder contains sections post model structure
  * Inside, dummy data folder, serve burned data to gives funcionality without database connection

* Front, its the public folder, contains html layout based in boilerplate.
    *Css, Javascript, and HTML was maked with the specifications given.
    * I do not use any framework and plugin, just a css gird and boilerplate.

* Helpers, implements a helper class to capsulate the view helpers and inject into the controller

* Views, implements the view content modules passed to main layout.

* Vendor, didn't use any package or framework, but i use composer auotoload to make easier classes
 and namespaces control and use.
 
### Previous open in your navigator please run composer update to satisfy requirements

Thank You

# Webwise

Este es un proyecto de test que implementa php para procesar dinamicamente la pagina del front-end de
webwise usando el patron MVC.

##What's inside

Es un ejemplo muy minimizado de un CRM

* Controller, Esta carpeta contiene los controladores, por el tipo de proyecto y 1 vista, se utilizó solo uno

* Models, contiene los modelos basados en la estructura de información a mostrar
  * Dentro se encuentra la carpeta dummydata, que contiene una clase que sirve los datos a mostrar, como
    si fuera una consulta SQL o pasando a través de un ORM

* Front, es la carpeta pública, contiene los archivos que pueden ser accedidos por el usuario.
    *Css, Javascript, y html fueron hechos con las especificaciones requeridas, por razones de 
    tiempo se dejó así pero puede ser mejorado considerablemente.
    * No se utilizaron frameworks ni librerias externas, excepto boilerplate, y una grilla css.

* Helpers, implementa librerias de ayuda para las vistas que se inyectan en el controlador

* Views, contiene los módulos de las vistas que se incluyen en el Layout principal.

* Vendor, no se usó ningún framework ni paquete para realizar esta prueba, solo se utilizó composer
  para cargar el autoload y facilitar el uso de clases y nombres.
 
### Antes de abrir en el navegador, asegurarse de ejecutar composer update

Muchas Gracias!
 
 